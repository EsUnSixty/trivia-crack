Accounts.ul.config({
    passwordSignFields: "USERNAME"
});

var APPLICATION_ID = "",
    SECRET_KEY = "",
    VERSION = 'v1';

Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);